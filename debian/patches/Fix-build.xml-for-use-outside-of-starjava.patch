From: Ole Streicher <olebole@debian.org>
Date: Wed, 15 Feb 2017 15:51:52 +0100
Subject: Fix build.xml for use outside of starjava

This includes the following changes:

 * Rename the `extclasspath` elements to `path`. With ant >= 1.6, there is no
   difference (and before, the difference was not relevant for Debian)

 * Ignore local property files

 * Change the starlink installation dir to the standard Java path /usr/share/java/

 * Prefix the name by `starlink-`

 * Adjust the build and test classpaths for Debian (also for CI tests)

 * Set a class path in the jar manifest

 * Set the source file encoding (cp1252), but not the source version (deprecated)

 * Don't sign the jarfile
---
 build.xml | 57 ++++++++++++++++++++++++---------------------------------
 1 file changed, 24 insertions(+), 33 deletions(-)

diff --git a/build.xml b/build.xml
index 7bed935..aad3993 100644
--- a/build.xml
+++ b/build.xml
@@ -37,12 +37,6 @@
  !-->
 
 <project name="Build file for UTIL" default="build" basedir=".">
-
-  <!-- If either or both of these files exist then any properties
-   !   contained within them will override those defined here.  -->
-  <property file="${user.home}/.stardev.properties"/>
-  <property file=".properties"/>
-
   <!-- Properties will also be set for all environment variables
    !   (PATH becomes "env.PATH"), generally not a good
    !   idea as names are OS dependent -->
@@ -55,13 +49,13 @@
    !-->
 
   <!-- Directory for the Starlink installation (usually /star/java)-->
-  <property name="star.dir" value="${basedir}/../../"/>
+  <property name="star.dir" value="/usr/share/java"/>
 
   <!-- Directory to install into (install target, usually /star/java)-->
   <property name="star.install" value="${star.dir}"/>
 
   <!-- Directory that contains the Starlink jar tree -->
-  <property name="star.jar.dir" value="${star.dir}/lib"/>
+  <property name="star.jar.dir" value="${star.dir}"/>
 
   <!-- Directory that contains the locally built sources (usually
    !   /star/java/source for full distribution) -->
@@ -82,8 +76,8 @@
    !-->
 
   <!-- Define the package name and current versions -->
-  <property name="Name" value="UTIL"/>
-  <property name="name" value="util"/>
+  <property name="Name" value="Starjava UTIL"/>
+  <property name="name" value="starlink-util"/>
   <property name="version" value="1.0"/>
 
   <!-- The Java package name -->
@@ -121,6 +115,7 @@
   <!-- Directory containing any third-party jars that should be
    !   distributed (normally these would belong in a proper package)-->
   <property name="src.jars.dir" value="${src.dir}/lib"/>
+  <mkdir dir="${src.jars.dir}"/>
 
   <!-- Directories for JUnit test cases and related files -->
   <property name="tests.dir" value="${src.dir}/testcases"/>
@@ -193,12 +188,14 @@
    !   class files in preference to installed ones (an extra user-define
    !   defined CLASSPATH can also be used as needed).
    !-->
-  <extclasspath id="installed.classpath">
-
-    <!-- JUnit -->
-    <pathelement location="${star.jar.dir}/junit/junit.jar"/>
+  <path id="installed.classpath">
+    <pathelement location="${star.jar.dir}/starlink-auth.jar"/>
+    <pathelement location="${star.jar.dir}/junit.jar"/>
+  </path>
 
-  </extclasspath>
+  <path id="jar.classpath">
+    <pathelement location="${dist.lib.pkg}/starlink-auth.jar"/>
+  </path>
 
   <!-- Generate the local build classpath. This is the most difficult
    !   part of handling the classpath. The local classes will be in
@@ -216,9 +213,7 @@
    !   doesn't work as expected add the additional classes/jars to
    !   the extra classpath.
    !-->
-  <extclasspath id="built.jarpath">
-    <pathelement location="${star.build.dir}/junit/lib/junit/junit.jar"/>
-  </extclasspath>
+  <path id="built.jarpath"/>
 
   <!-- "Local" third party jars. 
    !   They are installed with the package jar files and should be
@@ -278,6 +273,7 @@
     <pathelement location="${tests.dir}"/>
     <pathelement location="${tests.etc.dir}"/>
     <path refid="classpath"/>
+    <pathelement location="${star.jar.dir}/${name}.jar"/>
   </path>
 
   <!-- Turn this path into a string which is passed to the tests -->
@@ -349,7 +345,8 @@
            destdir="${build.classes}"
            debug="${debug}"
            deprecation="${deprecation}"
-           source="${source.version}"
+           encoding="cp1252"
+           includeantruntime="false"
            optimize="${optimize}">
 
       <compilerarg value="-Xlint:all,-path,-serial"/>
@@ -366,7 +363,7 @@
      !   (i.e. are discovered using "getResource()"). -->
     <copy todir="${build.classes}">
       <fileset dir="${java.dir}">
-        <include name="**/images/*"/>
+        <include name="**/images/*.gif"/>
         <include name="**/text/*"/>
       </fileset>
     </copy>
@@ -402,6 +399,10 @@
           description="-> creates the package jar file">
 
     <mkdir dir="${dist.lib.pkg}"/>
+    <manifestclasspath property="jar.class.path"
+                       jarfile="${dist.lib.pkg}/${name}.jar">
+      <classpath refid="jar.classpath" />
+    </manifestclasspath>
     <jar destfile="${dist.lib.pkg}/${name}.jar"
          basedir="${build.classes}">
       <manifest>
@@ -410,17 +411,6 @@
       </manifest>
     </jar>
 
-    <!-- Sign all jar files -->
-    <antcall target="signjars"/>
-  </target>
-
-  <target name="signjars" if="sign.jars">
-    <signjar alias="${webstart.alias}"
-             keystore="${webstart.keystore}"
-             keypass="${webstart.keypass}"
-             storepass="${webstart.storepass}">
-       <fileset dir="${dist.lib}" includes="**/*.jar **/*.zip"/>
-    </signjar>
   </target>
 
   <!--
@@ -832,7 +822,7 @@
              locale="en"
              windowtitle="${Name} API"
              doctitle="${Name}"
-             source="${source.version}"
+             encoding="cp1252"
              classpathref="classpath">
       <arg value="-Xdoclint:all,-missing"/>
       <arg value="-quiet"/>
@@ -917,7 +907,8 @@
     <javac srcdir="${tests.dir}"
            destdir="${build.tests}"
            debug="${debug}"
-           source="${source.version}"
+           encoding="cp1252"
+           includeantruntime="false"
            deprecation="${deprecation}" >
       <classpath refid="tests-classpath"/>
     </javac>
